List<Map<String, dynamic>> hotelList = [
  {
    'image': 'one.jpg',
    'place': 'Phnom Penh',
    'destination': 'London',
    'price': 25
  },
  {'image': 'two.jpg', 'place': 'Poipet', 'destination': 'Angkor', 'price': 25},
  {
    'image': 'three.jpg',
    'place': 'Lin\'s home',
    'destination': '3h 7\$',
    'price': 25
  }
];

List<Map<String, dynamic>> ticketList = [
  {
    'from': {'code': 'NYC', 'name': 'New-York'},
    'to': {'code': 'LDN', 'name': 'London'},
    'flying_time': '9H 30M',
    'date': '1 MAY',
    'departure_time': '09:00 AM',
    'number': 23
  },
  {
    'from': {'code': 'NYC', 'name': 'New-York'},
    'to': {'code': 'LDN', 'name': 'London'},
    'flying_time': '8H 30M',
    'date': '1 MAY',
    'departure_time': '08:00 AM',
    'number': 23
  },
  {
    'from': {'code': 'NYC', 'name': 'New-York'},
    'to': {'code': 'LDN', 'name': 'London'},
    'flying_time': '8H 30M',
    'date': '1 MAY',
    'departure_time': '08:00 AM',
    'number': 23
  },
];
