import 'package:flutter/material.dart';
import '../utils/app_style.dart';

class AppDoubleTextWidget extends StatelessWidget {
  const AppDoubleTextWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text("Upcoming Flights", style: Styles.headLineStyle2),
          InkWell(
            onTap: () {
              print("Yu");
            },
            child: Text(
              "View all",
              style: Styles.textStyle.copyWith(color: Styles.primaryColor),
            ),
          ),
        ],
      ),
    );
  }
}
